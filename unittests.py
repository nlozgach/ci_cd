import unittest
import pandas as pd
import numpy as np

class TestPandas(unittest.TestCase):
    PATH = 'athlete events.csv'

    def setUp(self):
        self.data = pd.read_csv(self.PATH)
    
    def test_youngest_male(self):
        data_olymp_male = self.data.loc[(self.data['Sex'] == "M") & ((
            self.data['Games'] == "1988 Winter") | (
                self.data['Games'] == "1988 Summer")) & self.data['Age'].notna()]
        result_olymp_male_young = data_olymp_male.sort_values(
            by='Age', ascending=False).tail(1)
        result = result_olymp_male_young.iloc[0]['Age']

        self.assertEqual(result, 12.0)

    def test_max_tennis(self):
        data_fem_basket = self.data.loc[(self.data['Sex'] == "F") & (
            self.data['Sport'] == "Tennis") & (
                self.data['Games'] == "1988 Summer") & self.data['Height'].notna()]
        max_value = data_fem_basket["Height"].max()
        result = np.around(max_value, 1)
        
        self.assertEqual(result, 190.0)

if __name__ == "__main__":
    unittest.main()